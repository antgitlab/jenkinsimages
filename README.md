```
https://hub.docker.com/r/jenkins/agent
https://hub.docker.com/r/jenkins/inbound-agent
https://hub.docker.com/r/jenkins/ssh-agent
https://catalog.redhat.com/software/containers/rhel7/57ea8cee9c624c035f96f3af?tag=7.6-362&push_date=1564418517000&container-tabs=gti

docker pull jenkins/agent:alpine-jdk11
docker pull jenkins/inbound-agent:4.7-1-jdk11-nanoserver-1809
docker pull jenkins/ssh-agent:nanoserver-1809-jdk8
docker pull registry.access.redhat.com/rhel7:7.6-362

docker save cd06036b8eb9 -o jenkins_agent.alphine-jdk11.tar
docker save 6d74c9e84a27 -o jenkins_inbound-agent.4.7-1-jdk11-nanoserver-1809.tar
docker save 1970311f4b29 -o jenkins-ssh-agent.nanoserver-1809-jdk8.tar
docker save 31cd91012c57 -o redhat.com.rhel7.7.6-362.tar

-rw-r--r--. 1 root root 377773568 Jul 21 09:44 jenkins_agent.alpine-jdk11.tar
-rw-r--r--. 1 root root 924602368 Jul 21 09:56 jenkins_inbound-agent.4.7-1-jdk11-nanoserver-1809.tar
-rw-r--r--. 1 root root 731225600 Jul 21 10:00 jenkins-ssh-agent.nanoserver-1809-jdk8.tar
-rw-r--r--. 1 root root 213813248 Jul 21 10:50 redhat.com.rhel7.7.6-362.tar

REPOSITORY                        TAG                          IMAGE ID      CREATED        SIZE
docker.io/jenkins/agent           alpine-jdk11                 cd06036b8eb9  27 hours ago   378 MB
docker.io/jenkins/inbound-agent   4.7-1-jdk11-nanoserver-1809  6d74c9e84a27  39 hours ago   924 MB
docker.io/jenkins/ssh-agent       nanoserver-1809-jdk8         1970311f4b29  41 hours ago   731 MB
registry.access.redhat.com/rhel7  7.6-362                      31cd91012c57  24 months ago  214 MB
```
